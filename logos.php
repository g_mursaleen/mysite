<?php include('include/header.php'); ?>
<body>
<div class="container">
  <h1 class="fw400 text-center text-uppercase">Logos</h1>
  <hr style="border-color:#666;">
  <div class="cxm-logos">
	<?php $i = 0; $col = 3; ?>
    <?php for($logo = 22; $logo > 0; $logo--){ ?>
    
    <?php if($i == 0) { ?> 
    <div class="row">
    <?php } ?>      
    
      <div class="col-sm-<?php echo 12/$col; ?>">
        <div class="well well-sm well-gm text-center"> <a href="media/logos/<?php echo $logo; ?>-logo.png" target="_blank"><img alt="Logo" src="media/logos/<?php echo $logo; ?>-logo.png"></a> </div>
      </div>
      
    <?php $i++; if($i == $col) { $i = 0; ?>
    </div>
    <?php } ?>
    
    <?php } ?>
    <?php if($i > 0) { ?> 
    </div>
    <?php }?>        
  </div>
  <?php include('include/categories.php'); ?>
</div>
<?php include('include/footer.php'); ?>
</body>
</html>
