'use strict';
angular.module('cxmPortfolio').config(['$routeProvider',
  function config($routeProvider) {
    $routeProvider.
      when('/', {
        template: '<home-pg></home-pg>'

      }).
      when('/projects', {
        template: '<project-pg></project-pg>'

      }).
      when('/web-application', {
        template: '<web-app-pg></web-app-pg>'

      }).
      when('/mobile-application', {
        template: '<mob-app-pg></mob-app-pg>'

      }).
      when('/html-email', {
        template: '<html-email-pg></html-email-pg>'

      }).
      when('/logos', {
        template: '<logo-pg></logo-pg>'

      }).
      when('/business-card', {
        template: '<business-card-pg></business-card-pg>'

      }).
      otherwise('/');
  }
]);