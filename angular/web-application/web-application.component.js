'use strict';
angular.module('webAppPg').component('webAppPg', {
    templateUrl: 'web-application/web-application-pg.template.html',
    controller: ['$http',
      function webAppPgController() {
        var self = this;
        self.heading = 'Web Application';
        self.subheading = 'Screenshot Of Web Applications';

        self.webApps = [
            {
              id: 'carousel-web-app-1',
              name: 'App 1',
              imgs: [
                '../media/screen-shot/tip/1-login.jpg',
                '../media/screen-shot/tip/2-home.jpg',
                '../media/screen-shot/tip/3-main-menu.jpg'
              ]
            },
            {
              id: 'carousel-web-app-2',
              name: 'App 2',
              imgs: [
                '../media/screen-shot/ke/1-login.jpg',
                '../media/screen-shot/ke/2-home.jpg',
                '../media/screen-shot/ke/3-user-list.jpg',
                '../media/screen-shot/ke/4-case-list.jpg',
                '../media/screen-shot/ke/5-case-add-edit.jpg',
                '../media/screen-shot/ke/6-region-add.jpg',
                '../media/screen-shot/ke/7-assign-task.jpg',
                '../media/screen-shot/ke/8-report.jpg'
              ]
            },
            {
              id: 'carousel-web-app-3',
              name: 'App 3',
              imgs: [
                '../media/screen-shot/lenovo/1-login.jpg',
                '../media/screen-shot/lenovo/2-home.jpg',
                '../media/screen-shot/lenovo/3-report.jpg'
              ]
            },
        ];      
      }
    ]
});