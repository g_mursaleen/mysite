'use strict';
angular.module('htmlEmailPg').component('htmlEmailPg', {
    templateUrl: 'html-email/html-email-pg.template.html',
    controller: ['$http',
      function htmlEmailPgController() {
        var self = this;
        self.heading = 'HTML Email';
        self.subheading = 'Please Click on image for more detail';

        self.htmlEmails = [
            {
              name: 'financial3 Newsletter',
              imgUrl: '../media/email/1-email.png',
              link: 'http://financial3.wpengine.com/newsletter/2015/march'              
            },
            {
              name: 'Outdoor Living Jindalee Newsletter',
              imgUrl: '../media/email/1-email.png',
              link: 'http://www.outdoorlivingjindalee.com.au/newsletter/2015/february'              
            },
            {
              name: 'Our VIP Customer',
              imgUrl: '../media/email/1-email.png',
              link: 'http://www.ourvipcustomer.com/news-letter/happy-hockers/2014/january/'              
            },
            {
              name: 'Our VIP Customer',
              imgUrl: '../media/email/1-email.png',
              link: 'http://www.ourvipcustomer.com/news-letter/qpaint/2014/june'              
            },
            {
              name: 'financial3 Newsletter',
              imgUrl: '../media/email/1-email.png',
              link: 'http://financial3.wpengine.com/newsletter/2015/march'              
            },
            {
              name: 'Our VIP Customer',
              imgUrl: '../media/email/1-email.png',
              link: 'http://www.ourvipcustomer.com/news-letter/happy-hockers/2014/january/'              
            }
        ];      
      }
    ]
});