'use strict';
angular.module('homePg').component('homePg', {
    templateUrl: 'home/home-pg.template.html',
    controller: ['$http',
      function HomePgController() {
        var self = this;
        self.heading = 'Portfolio';
        self.subheading = 'Please Follow below link for more detail';

        self.pages = [
            {name: 'Online Projects', imgUrl: '../media/web-bw.png', link: '#!/projects'},
            {name: 'Web Application', imgUrl: '../media/web-bw.png', link: '#!/web-application'},
            {name: 'Mobile Application', imgUrl: '../media/mobile-bw.png', link: '#!/mobile-application'},
            {name: 'HTML Email', imgUrl: '../media/email-bw.png', link: '#!/html-email'},
            {name: 'Logos', imgUrl: '../media/logo-bw.png', link: '#!/logos'},
            {name: 'Business Card', imgUrl: '../media/card-bw.png', link: '#!/business-card'},
        ];      
      }
    ]
});