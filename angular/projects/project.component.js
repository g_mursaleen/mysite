'use strict';
angular.module('projectPg').component('projectPg', {
    templateUrl: 'projects/project-pg.template.html',
    controller: ['$http',
      function projectPgController($http) {
        var self = this;
        self.currentPage = 1;
        self.pageSize = 20;
        self.heading = 'Online Projects';
        self.subheading = 'Please Follow below link for more detail';

        $http.get('projects.php').then(function(response) {
          self.projects = response.data;
        });
        self.pageChangeHandler = function(pgNum) {
          console.log('going to page ' + pgNum);
        };        
      }
    ]
});