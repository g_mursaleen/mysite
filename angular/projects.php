<?php
$projects = array(
	array(
		'name' => 'Anthive - Paperless Exam Prep',
		'cat' => 'WP',
		'img' => 'anthive.jpg',
		'link' => 'http://anthive.co/'
	),
	array(
		'name' => 'Jems Education and Visa Consultancy',
		'cat' => 'WP',
		'img' => 'jemsvisa.jpg',
		'link' => 'http://www.jemsvisa.com'
	),
	array(
		'name' => 'moykawear',
		'cat' => 'HTML, CSS, PHP',
		'img' => 'moykawear.jpg',
		'link' => 'http://www.moykawear.com'
	),
	array(
		'name' => 'We Contact',
		'cat' => 'WP',
		'img' => 'wecontact.jpg',
		'link' => 'http://wecontact.com'
	),
    array(
		'name' => 'Lethal Leather',
		'cat' => 'WP, Woocommerce',
		'img' => 'lethalleather.jpg',
		'link' => 'http://lethalleather.com'
	),
	array(
		'name' => 'USHLBE',
		'cat' => 'WP',
		'img' => 'ushlbe_education.jpg',
		'link' => 'https://www.ushlbe.education'
	),
	array(
		'name' => 'Avayasana',
		'cat' => 'WP',
		'img' => 'avayasana.jpg',
		'link' => 'https://www.avayasana.com'
	),
    array(
		'name' => 'APP Junke',
		'cat' => 'BS4, PHP',
		'img' => 'appjunke.jpg',
		'link' => 'http://appjunke.com'
	),
	array(
		'name' => 'SFC Learnings',
		'cat' => 'WP',
		'img' => 'sfclearnings.jpg',
		'link' => 'http://www.sfclearnings.com'
	),
	array(
		'name' => 'NITI',
		'cat' => 'WP',
		'img' => 'niti.jpg',
		'link' => 'http://www.niti.edu.sa'
	),
	array(
		'name' => 'RevolutionX',
		'cat' => 'WP',
		'img' => 'revolutionx.jpg',
		'link' => 'http://revolutionx.ai'
	),
	array(
		'name' => 'App Tech',
		'cat' => 'WP',
		'img' => 'apptech.jpg',
		'link' => 'https://apptech.com.tr'
	),
	array(
		'name' => 'GABA Tea',
		'cat' => 'WP, Woocommerce',
		'img' => 'gabatea.jpg',
		'link' => 'https://gabatea.asia'
	),
	array(
		'name' => 'Digital Uniq',
		'cat' => 'WP',
		'img' => 'mydigitaluniq.jpg',
		'link' => 'http://mydigitaluniq.com'
	),
	array(
		'name' => 'Hasrina Hakimi',
		'cat' => 'WP',
		'img' => 'hasrinahakimico.jpg',
		'link' => 'https://hasrinahakimico.com'
	),
	array(
		'name' => 'The 3W Solutions',
		'cat' => 'WP',
		'img' => 'the3wsolutions.jpg',
		'link' => 'https://the3wsolutions.com'
	),
	array(
		'name' => 'American Punch',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'americanpunch.jpg',
		'link' => 'https://americanpunch.com'
	),
	array(
		'name' => 'Property Club',
		'cat' => 'WP',
		'img' => 'propertyclub.jpg',
		'link' => 'http://propertyclub.biz'
	),	
	array(
		'name' => 'The Nano Tech',
		'cat' => 'WP',
		'img' => 'thenanotech.jpg',
		'link' => 'http://www.thenanotech.co.uk'
	),
	array(
		'name' => 'FIRA Soft Technologies',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'firasofttechnologies.jpg',
		'link' => 'http://www.firasofttechnologies.com'
	),
	array(
		'name' => 'The UK Energy',
		'cat' => 'WP',
		'img' => 'ukenergy.jpg',
		'link' => 'http://www.theukenergy.com'
	),
	array(
		'name' => 'JemsTech',
		'cat' => 'WP',
		'img' => 'jemstech.jpg',
		'link' => 'http://jemstech.net'
	),
	array(
		'name' => 'US Higher Learning Board For Education',
		'cat' => 'WP',
		'img' => 'ushlbe.jpg',
		'link' => 'http://ushlbe.org'
	),
	array(
		'name' => 'Way For Learning',
		'cat' => 'WP',
		'img' => 'wayforlearning.jpg',
		'link' => 'http://www.wayforlearning.com'
	),
	array(
		'name' => 'We Bridge Solutions',
		'cat' => 'WP',
		'img' => 'webridgesolutions.jpg',
		'link' => 'http://www.webridgesolutions.com'
	),array(
		'name' => 'Nex Reviews',
		'cat' => 'WP',
		'img' => 'nexreviews.jpg',
		'link' => 'https://nexreviews.com'
	),
	array(
		'name' => 'Bree Cloud',
		'cat' => 'WP',
		'img' => 'breecloud.jpg',
		'link' => 'https://breecloud.com'
	),
	array(
		'name' => 'The Cloud Workshop',
		'cat' => 'WP',
		'img' => 'thecloudworkshop.jpg',
		'link' => 'https://www.thecloudworkshop.com'
	),
	array(
		'name' => 'Aab Telecom',
		'cat' => 'WP',
		'img' => 'aabtelecom.jpg',
		'link' => 'http://aabtelecom.com'
	),
	array(
		'name' => 'Bi Jingo',
		'cat' => 'WP',
		'img' => 'bi-jingo.jpg',
		'link' => 'http://bi-jingo.com'
	),
	array(
		'name' => 'Randell Commercial',
		'cat' => 'WP',
		'img' => 'randellcommercial.jpg',
		'link' => 'http://www.randellcommercial.uk'
	),
	array(
		'name' => 'Iskin Care',
		'cat' => 'WP',
		'img' => 'iskin-care.jpg',
		'link' => 'https://iskin-care.co.uk'
	),
	array(
		'name' => 'Unique H2O',
		'cat' => 'WP',
		'img' => 'uniqueh2o.jpg',
		'link' => 'http://www.uniqueh2o.com.au'
	),
	array(
		'name' => 'The Clean Boot',
		'cat' => 'WP',
		'img' => 'thecleanboot.jpg',
		'link' => 'https://www.thecleanboot.com'
	),
	array(
		'name' => 'Sculpt Advisors',
		'cat' => 'WP',
		'img' => 'sculptadvisors.jpg',
		'link' => 'http://www.sculptadvisors.com'
	),
	array(
		'name' => 'HCS Pak',
		'cat' => 'WP',
		'img' => 'hcspak.jpg',
		'link' => 'http://hcspak.com'
	),
	array(
		'name' => 'International Mech',
		'cat' => 'WP',
		'img' => 'internationalmech.jpg',
		'link' => 'http://www.internationalmech.com'
	),
	array(
		'name' => 'JMS Cleaning Sarasota',
		'cat' => 'WP',
		'img' => 'jmscleaningsarasota.jpg',
		'link' => 'http://jmscleaningsarasota.com'
	),
	array(
		'name' => 'Get Set For High School',
		'cat' => 'WP',
		'img' => 'getsetforhighschool.jpg',
		'link' => 'http://www.getsetforhighschool.com.au'
	),
	array(
		'name' => 'Marie Houlden',
		'cat' => 'WP',
		'img' => 'mariehoulden.jpg',
		'link' => 'https://mariehoulden.com'
	),
	array(
		'name' => 'Manor House Plants',
		'cat' => 'WP',
		'img' => 'manorhouseplants.jpg',
		'link' => 'http://www.manorhouseplants.co.uk'
	),
	array(
		'name' => 'Eclat',
		'cat' => 'WP',
		'img' => 'eclat.jpg',
		'link' => 'https://eclat.co.uk'
	),
	array(
		'name' => 'Tricia Walker',
		'cat' => 'WP',
		'img' => 'triciawalker.jpg',
		'link' => 'https://www.triciawalker.co.uk'
	),
	array(
		'name' => 'The Crown Chiddingfold',
		'cat' => 'WP',
		'img' => 'thecrownchiddingfold.jpg',
		'link' => 'http://thecrownchiddingfold.com'
	),
	array(
		'name' => 'Port Douglas Day Spa',
		'cat' => 'WP',
		'img' => 'portdouglasdayspa.jpg',
		'link' => 'http://www.portdouglasdayspa.com'
	)/*,
	http://www.smartwiredservices.com.au/
	http://www.theprofitinstitute.com.au/
	array(
		name' => 'Christ Chur Chuk IP',
		cat' => 'WP',
		img' => 'christchurchukip.jpg',
		link' => 'http://christchurchukip.com/'
	)*/,
	array(
		'name' => 'Western Aparts',
		'cat' => 'BS,PHP',
		'img' => 'westernaparts.jpg',
		'link' => 'https://westernaparts.com'
	),
	array(
		'name' => 'We Buy Van Today',
		'cat' => 'BS,PHP',
		'img' => 'webuyvantoday.jpg',
		'link' => 'https://webuyvantoday.co.uk'
	),
	array(
		'name' => 'London House',
		'cat' => 'BS,PHP',
		'img' => 'london-house.jpg',
		'link' => 'https://london-house.com'
	),
	array(
		'name' => 'Sale 4 Trade',
		'cat' => 'BS,PHP',
		'img' => 'sale4trade.jpg',
		'link' => 'http://www.sale4trade.co.uk'
	),
	array(
		'name' => 'The Clean Plumber',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'thecleanplumber.jpg',
		'link' => 'http://www.mxgpfreebie.com.au/'
	),
	array(
		'name' => 'MXGP Freebie',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'mxgpfreebie.jpg',
		'link' => 'http://www.mxgpfreebie.com.au'
	),
	array(
		'name' => 'Business Wealth Builders',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'businesswealthbuilders.jpg',
		'link' => 'http://businesswealthbuilders.com.au'
	),
	array(
		'name' => 'ABA White Rooms',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'abawhiterooms.jpg',
		'link' => 'https://www.abawhiterooms.co.uk'
	),
	array(
		'name' => 'Henny Donovan Motif',
		'cat' => 'WP',
		'img' => 'hennydonovanmotif.jpg',
		'link' => 'https://hennydonovanmotif.co.uk'
	),
	array(
		'name' => 'Milton Architects',
		'cat' => 'WP',
		'img' => 'miltonarchitects.jpg',
		'link' => 'http://miltonarchitects.co.uk'
	),
	array(
		'name' => 'Multitron',
		'cat' => 'WP',
		'img' => 'multitron.jpg',
		'link' => 'http://multitron.co.uk'
	),
	array(
		'name' => 'Alchemy Award',
		'cat' => 'WP',
		'img' => 'alchemyaward.jpg',
		'link' => 'http://alchemyaward.com'
	),
	array(
		'name' => 'Park Share',
		'cat' => 'BS,PHP',
		'img' => 'parkshare.jpg',
		'link' => 'https://parkshare.net'
	),
	array(
		'name' => 'The Motivation Agency',
		'cat' => 'BS,PHP',
		'img' => 'themotivationagency.jpg',
		'link' => 'http://www.themotivationagency.co.uk'
	),
	array(
		'name' => 'AS Creative',
		'cat' => 'BS,PHP,ANIMATION',
		'img' => 'ascreative.jpg',
		'link' => 'http://www.ascreative.com'
	),
	/*array(
		'name' => 'OPE Solutions',
		'cat' => 'html,php',
		'img' => 'ope.jpg',
		'link' => 'http://www.ope.solutions'
	),
	array(
		'name' => 'Media Lawyers UK',
		'cat' => 'wp',
		'img' => 'medialawyersuk.jpg',
		'link' => 'http://www.medialawyersuk.com'
	),	
	*/
	array(
		'name' => 'Hrandell Properties',
		'cat' => 'BS,PHP',
		'img' => 'hrandellproperties.jpg',
		'link' => 'http://www.hrandellproperties.co.uk'
	),
	array(
		'name' => 'Auto Associates Japan',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'aajt.jpg',
		'link' => 'http://aajt.co.jp'
	),
	array(
		'name' => 'Shaheen Insurance',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'shaheeninsurance.jpg',
		'link' => 'http://www.shaheeninsurance.com'
	),
	array(
		'name' => 'TAG Group',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'taggroup.jpg',
		'link' => 'https://www.taggroup.co'
	),
	array(
		'name' => 'Swallowing Test',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'swallowingtest.jpg',
		'link' => 'http://www.swallowingtest.com'
	),
	array(
		'name' => 'Jsaunder Sandsons Flooring',
		'cat' => 'WP',
		'img' => 'jsaundersandsonsflooring.jpg',
		'link' => 'http://www.jsaundersandsonsflooring.co.uk'
	),
	array(
		'name' => 'Euphorbia Design',
		'cat' => 'WP',
		'img' => 'euphorbia-design.jpg',
		'link' => 'http://euphorbia-design.co.uk'
	),
	array(
		'name' => 'Vision TP',
		'cat' => 'WP',
		'img' => 'visiontp.jpg',
		'link' => 'http://www.visiontp.co.uk'
	),
	array(
		'name' => 'Harkness Kennett',
		'cat' => 'WP',
		'img' => 'harknesskennett.jpg',
		'link' => 'http://www.harknesskennett.com/'
	),
	array(
		'name' => 'We Buy Motors',
		'cat' => 'BS,PHP',
		'img' => 'webuymotors.jpg',
		'link' => 'https://www.webuymotors.com'
	),
	array(
		'name' => 'Payne Coaching',
		'cat' => 'BS,ANIMATION,PHP',
		'img' => 'paynecoaching.jpg',
		'link' => 'http://paynecoaching.com'
	),
	array(
		'name' => 'Financial 3',
		'cat' => 'BS,PHP',
		'img' => 'financial3.jpg',
		'link' => 'http://financial3.com.au'
	),
	array(
		'name' => 'Icon',
		'cat' => 'BS,PHP',
		'img' => 'iconcpl.jpg',
		'link' => 'https://www.iconcpl.com'
	),
	array(
		'name' => 'TAGM',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'tagm.jpg',
		'link' => 'http://www.tagm.co'
	),
	array(
		'name' => 'TAG Consulting',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'tagcon.jpg',
		'link' => 'http://tagcon.taggroup.co'
	),
	array(
		'name' => 'Kestrel &amp; Buzzards',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'knb.jpg',
		'link' => 'http://knb.tagglobalservices.com'
	),
	array(
		'name' => 'TAG Technologies',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'tagtechnologies.jpg',
		'link' => 'https://tagtechnologies.co'
	),
	array(
		'name' => 'Autumn Years Living',
		'cat' => 'WP',
		'img' => 'autumnyearsliving.jpg',
		'link' => 'http://www.autumnyearsliving.com'
	),
	array(
		'name' => 'Broad Water Park Golf',
		'cat' => 'WP',
		'img' => 'broadwaterparkgolf.jpg',
		'link' => 'http://www.broadwaterparkgolf.co.uk'
	),
	array(
		'name' => 'Steel Supplies',
		'cat' => 'WP',
		'img' => 'steelsuppliescharterstowers.jpg',
		'link' => 'http://steelsuppliescharterstowers.com.au'
	),
	array(
		'name' => 'Competency Master',
		'cat' => 'WP',
		'img' => 'competencymaster.jpg',
		'link' => 'https://competencymaster.com'
	),
	array(
		'name' => 'Proactive Edge',
		'cat' => 'WP',
		'img' => 'proactive-edge.jpg',
		'link' => 'http://www.proactive-edge.com.au'
	),
	array(
		'name' => 'Aqua Fix Plumbing',
		'cat' => 'WP',
		'img' => 'aquafixplumbing.jpg',
		'link' => 'http://aquafixplumbing.net.au'
	),
	array(
		'name' => 'The Business Mentor',
		'cat' => 'WP',
		'img' => 'thebusinessmentor.jpg',
		'link' => 'http://thebusinessmentor.com.au'
	),
	array(
		'name' => 'Mediregs',
		'cat' => 'WP',
		'img' => 'mediregs.jpg',
		'link' => 'https://mediregs.eu'
	),/*
	array(
		'name' => 'Vorson',
		'cat' => 'BS,PHP',
		'img' => 'vorson.jpg',
		'link' => 'http://www.vorson.net'
	),*/
	array(
		'name' => 'SNS Pak',
		'cat' => 'BS,PHP',
		'img' => 'snspak.jpg',
		'link' => 'http://www.snspak.com'
	),
	array(
		'name' => 'Outdoor Living Jindalee',
		'cat' => 'BS,PHP',
		'img' => 'outdoorlivingjindalee.jpg',
		'link' => 'http://www.outdoorlivingjindalee.com.au'
	),
	array(
		'name' => 'First Fitness',
		'cat' => 'BS,PHP',
		'img' => 'firstfitness.jpg',
		'link' => 'http://www.firstfitness.com'
	),
	array(
		'name' => 'Barker Graves',
		'cat' => 'BS,PHP',
		'img' => 'barkergraves.jpg',
		'link' => 'http://www.barkergraves.co.uk'
	),
	array(
		'name' => 'Fly It',
		'cat' => 'HTML,CSS,PHP',
		'img' => 'flyp-it.jpg',
		'link' => 'http://www.flyp-it.com'
	),
	array(
		'name' => 'Maktab A Roomi',
		'cat' => 'Magento',
		'img' => 'maktabaroomi.jpg',
		'link' => 'http://www.maktabaroomi.com'
	),
	array(
		'name' => 'Night Life Hero',
		'cat' => 'Magento',
		'img' => 'nightlifehero.jpg',
		'link' => 'https://www.nightlifehero.com'
	)
		
);
echo json_encode($projects);
?>