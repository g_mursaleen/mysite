'use strict';
angular.module('logoPg').component('logoPg', {
    templateUrl: 'logos/logo-pg.template.html',
    controller: ['$http',
      function logoPgController() {
        var self = this;
        self.heading = 'Logos';
        self.subheading = 'Please See logo images';

        self.logos = [
            {
              name: 'Way For Learning',
              imgUrl: '../media/logos/22-logo.png'              
            },
            {
              name: '3W Solutions',
              imgUrl: '../media/logos/21-logo.png'              
            },
            {
              name: 'FIRA Soft Technologies',
              imgUrl: '../media/logos/20-logo.png'              
            },
            {
              name: 'Deep Cleaner 1',
              imgUrl: '../media/logos/19-logo.png'              
            },
            {
              name: 'Deep Cleaner 2',
              imgUrl: '../media/logos/18-logo.png'              
            },
            {
              name: 'The Cloud Workshop',
              imgUrl: '../media/logos/17-logo.png'              
            },
            {
              name: 'Bree Cloud',
              imgUrl: '../media/logos/16-logo.png'              
            },
            {
              name: 'Nex Reviews',
              imgUrl: '../media/logos/15-logo.png'              
            },
            {
              name: 'Nex Resume',
              imgUrl: '../media/logos/14-logo.png'              
            },
            {
              name: 'Nex Devops',
              imgUrl: '../media/logos/13-logo.png'              
            },
            {
              name: 'MAYA',
              imgUrl: '../media/logos/12-logo.png'              
            },
            {
              name: 'K&B Studios',
              imgUrl: '../media/logos/11-logo.png'              
            },
            {
              name: 'TAG Consulting',
              imgUrl: '../media/logos/10-logo.png'              
            },
            {
              name: 'TY Traders',
              imgUrl: '../media/logos/9-logo.png'              
            },
            {
              name: 'VORSON',
              imgUrl: '../media/logos/8-logo.png'              
            },
            {
              name: 'Omega',
              imgUrl: '../media/logos/7-logo.png'              
            },
            {
              name: 'Kestrel & Buzzard',
              imgUrl: '../media/logos/6-logo.png'              
            },
            {
              name: 'Trade Lines',
              imgUrl: '../media/logos/5-logo.png'              
            },
            {
              name: 'FLAIR Logistics',
              imgUrl: '../media/logos/4-logo.png'              
            },
            {
              name: 'My Lover Finder',
              imgUrl: '../media/logos/3-logo.png'              
            },
            {
              name: 'TAG Technologies',
              imgUrl: '../media/logos/2-logo.png'              
            },
            {
              name: 'Icon Consultants',
              imgUrl: '../media/logos/1-logo.png'              
            }            
        ];      
      }
    ]
});