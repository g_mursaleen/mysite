'use strict';
angular.module('mobAppPg').component('mobAppPg', {
    templateUrl: 'mobile-application/mobile-application-pg.template.html',
    controller: ['$http',
      function mobAppPgController() {
        var self = this;
        self.heading = 'Mobile Application';
        self.subheading = 'Screenshot Of Web Applications';

        self.mobApps = [
            {
              id: 'carousel-mob-app-1',
              name: 'Mobile App 1',
              imgs: [
                '../media/screen-shot/tip-app/1-login.jpg',
                '../media/screen-shot/tip-app/2-home.jpg'
              ]
            },
            {
              id: 'carousel-mob-app-2',
              name: 'Mobile App 2',
              imgs: [
                '../media/screen-shot/ke-app/1-login.jpg',
                '../media/screen-shot/ke-app/2-billing.jpg',
                '../media/screen-shot/ke-app/3-case.jpg',
                '../media/screen-shot/ke-app/4-case-detail.jpg',
                '../media/screen-shot/ke-app/5-popup.jpg',
                '../media/screen-shot/ke-app/6-queue-data.jpg'
              ]
            },
            {
              id: 'carousel-mob-app-3',
              name: 'Moblie App 3',
              imgs: [
                '../media/screen-shot/lenovo-app/1-login.jpg',
                '../media/screen-shot/lenovo-app/2-home.jpg',
                '../media/screen-shot/lenovo-app/3-sellout.jpg'
              ]
            },
        ];      
      }
    ]
});