'use strict';
angular.module('businessCardPg').component('businessCardPg', {
    templateUrl: 'business-card/business-card-pg.template.html',
    controller: ['$http',
      function businessCardPgController() {
        var self = this;
        self.heading = 'Business Card';
        self.subheading = 'Please See Business Card images';

        self.businessCards = [
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/1-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/2-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/3-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/4-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/5-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/6-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/7-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/8-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/9-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/10-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/11-card.png'
          },
          {
            name: 'Business Card',
            imgUrl: '../media/business-card/12-card.png'
          }
        ];      
      }
    ]
});