<?php include('include/header.php'); ?>
<body>
<div class="container">
  <h1 class="fw400 text-center text-uppercase">Mobile Applications</h1>
  <hr style="border-color:#666;">
  <p class="lead text-center">Screenshot Of Mobile Applications</p>
  <div class="row">
    <div class="col-sm-12">
      <div class="well well-sm well-gm text-center">
        <div id="carousel-mob-app-1" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carousel-mob-app-1" data-slide-to="0" class=""></li>
            <li data-target="#carousel-mob-app-1" data-slide-to="1" class=""></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active"> <a href="media/screen-shot/tip-app/1-login.jpg" target="_blank"><img class="center-block" alt="Login" src="media/screen-shot/tip-app/1-login.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/tip-app/2-home.jpg" target="_blank"><img class="center-block" alt="Home" src="media/screen-shot/tip-app/2-home.jpg"></a> </div>
          </div>
          <a class="left carousel-control" href="#carousel-mob-app-1" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-mob-app-1" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      </div>
      <div class="well well-sm well-gm text-center">
        <div id="carousel-mob-app-2" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carousel-mob-app-2" data-slide-to="0" class=""></li>
            <li data-target="#carousel-mob-app-2" data-slide-to="1" class=""></li>
            <li data-target="#carousel-mob-app-2" data-slide-to="2" class=""></li>
            <li data-target="#carousel-mob-app-2" data-slide-to="3" class=""></li>
            <li data-target="#carousel-mob-app-2" data-slide-to="4" class=""></li>
            <li data-target="#carousel-mob-app-2" data-slide-to="5" class=""></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active"> <a href="media/screen-shot/ke-app/1-login.jpg" target="_blank"><img class="center-block" alt="Login" src="media/screen-shot/ke-app/1-login.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke-app/2-billing.jpg" target="_blank"><img class="center-block" alt="Home" src="media/screen-shot/ke-app/2-billing.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke-app/3-case.jpg" target="_blank"><img class="center-block" alt="Home" src="media/screen-shot/ke-app/3-case.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke-app/4-case-detail.jpg" target="_blank"><img class="center-block" alt="Home" src="media/screen-shot/ke-app/4-case-detail.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke-app/5-popup.jpg" target="_blank"><img class="center-block" alt="Home" src="media/screen-shot/ke-app/5-popup.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke-app/6-queue-data.jpg" target="_blank"><img class="center-block" alt="Home" src="media/screen-shot/ke-app/6-queue-data.jpg"></a> </div>
          </div>
          <a class="left carousel-control" href="#carousel-mob-app-2" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-mob-app-2" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      </div>
      <div class="well well-sm well-gm text-center">
        <div id="carousel-mob-app-3" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carousel-mob-app-3" data-slide-to="0" class=""></li>
            <li data-target="#carousel-mob-app-3" data-slide-to="1" class=""></li>
            <li data-target="#carousel-mob-app-3" data-slide-to="2" class=""></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active"> <a href="media/screen-shot/lenovo-app/1-login.jpg" target="_blank"><img class="center-block" alt="Login" src="media/screen-shot/lenovo-app/1-login.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/lenovo-app/2-home.jpg" target="_blank"><img class="center-block" alt="Home" src="media/screen-shot/lenovo-app/2-home.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/lenovo-app/3-sellout.jpg" target="_blank"><img class="center-block" alt="Home" src="media/screen-shot/lenovo-app/3-sellout.jpg"></a> </div>
          </div>
          <a class="left carousel-control" href="#carousel-mob-app-3" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-mob-app-3" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      </div>
    </div>
  </div>
  <?php include('include/categories.php'); ?>
</div>
<?php include('include/footer.php'); ?>
</body>
</html>
