<?php include('include/header.php'); ?>
<body>
<div class="container">
  <h1 class="fw400 text-center text-uppercase">Web Applications</h1>
  <hr style="border-color:#666;">
  <p class="lead text-center">Screenshot Of Web Applications</p>
  <div class="row">
    <div class="col-sm-12">
      <div class="well well-sm well-gm text-center">
        <div id="carousel-web-app-4" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carousel-web-app-4" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-web-app-4" data-slide-to="1" class=""></li>
            <li data-target="#carousel-web-app-4" data-slide-to="2" class=""></li>
            <li data-target="#carousel-web-app-4" data-slide-to="3" class=""></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active"> <a href="media/screen-shot/rgs/1-login.jpg" target="_blank"><img alt="Login" src="media/screen-shot/rgs/1-login.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/rgs/2-home.jpg" target="_blank"><img alt="Home" src="media/screen-shot/rgs/2-home.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/rgs/3-table.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/rgs/3-table.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/rgs/4-search.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/rgs/4-search.jpg"></a> </div>
          </div>
          <a class="left carousel-control" href="#carousel-web-app-4" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-web-app-4" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      </div>

      <div class="well well-sm well-gm text-center">
        <div id="carousel-web-app-1" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carousel-web-app-1" data-slide-to="0" class=""></li>
            <li data-target="#carousel-web-app-1" data-slide-to="1" class=""></li>
            <li data-target="#carousel-web-app-1" data-slide-to="2" class="active"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active"> <a href="media/screen-shot/tip/1-login.jpg" target="_blank"><img alt="Login" src="media/screen-shot/tip/1-login.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/tip/2-home.jpg" target="_blank"><img alt="Home" src="media/screen-shot/tip/2-home.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/tip/3-main-menu.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/tip/3-main-menu.jpg"></a> </div>
          </div>
          <a class="left carousel-control" href="#carousel-web-app-1" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-web-app-1" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      </div>
      <div class="well well-sm well-gm text-center">
        <div id="carousel-web-app-2" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carousel-web-app-2" data-slide-to="0" class=""></li>
            <li data-target="#carousel-web-app-2" data-slide-to="1" class=""></li>
            <li data-target="#carousel-web-app-2" data-slide-to="2" class="active"></li>
            <li data-target="#carousel-web-app-2" data-slide-to="3" class="active"></li>
            <li data-target="#carousel-web-app-2" data-slide-to="4" class="active"></li>
            <li data-target="#carousel-web-app-2" data-slide-to="5" class="active"></li>
            <li data-target="#carousel-web-app-2" data-slide-to="6" class="active"></li>
            <li data-target="#carousel-web-app-2" data-slide-to="7" class="active"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active"> <a href="media/screen-shot/ke/1-login.jpg" target="_blank"><img alt="Login" src="media/screen-shot/ke/1-login.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke/2-home.jpg" target="_blank"><img alt="Home" src="media/screen-shot/ke/2-home.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke/3-user-list.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/ke/3-user-list.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke/4-case-list.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/ke/4-case-list.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke/5-case-add-edit.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/ke/5-case-add-edit.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke/6-region-add.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/ke/6-region-add.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke/7-assign-task.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/ke/7-assign-task.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/ke/8-report.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/ke/8-report.jpg"></a> </div>
          </div>
          <a class="left carousel-control" href="#carousel-web-app-2" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-web-app-2" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      </div>
      <div class="well well-sm well-gm text-center">
        <div id="carousel-web-app-3" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carousel-web-app-3" data-slide-to="0" class=""></li>
            <li data-target="#carousel-web-app-3" data-slide-to="1" class=""></li>
            <li data-target="#carousel-web-app-3" data-slide-to="2" class="active"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active"> <a href="media/screen-shot/lenovo/1-login.jpg" target="_blank"><img alt="Login" src="media/screen-shot/lenovo/1-login.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/lenovo/2-home.jpg" target="_blank"><img alt="Home" src="media/screen-shot/lenovo/2-home.jpg"></a> </div>
            <div class="item"> <a href="media/screen-shot/lenovo/3-report.jpg" target="_blank"><img alt="Menu" src="media/screen-shot/lenovo/3-report.jpg"></a> </div>
          </div>
          <a class="left carousel-control" href="#carousel-web-app-3" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-web-app-3" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
      </div>
    </div>
  </div>
  <?php include('include/categories.php'); ?>
</div>
<?php include('include/footer.php'); ?>
</body>
</html>
