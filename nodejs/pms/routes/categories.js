var express = require('express');
const bodyParser = require('body-parser');
// const { check, validationResult } = require('express-validator');
// const { matchedData, sanitizeBody } = require('express-validator');
// const bcrypt = require('bcryptjs');
// const jwt = require('jsonwebtoken');
var chkLogin = require('../modules/chk-login');
var categoryModel = require('../modules/category');
var router = express.Router();

const categories = categoryModel.find({});

// if (typeof localStorage === "undefined" || localStorage === null) {
//   var LocalStorage = require('node-localstorage').LocalStorage;
//   localStorage = new LocalStorage('./scratch');
// }

/* GET Categories page. */
router.get('/', chkLogin, function(req, res, next) {
  var paginateOptions = {
    // pagination: false,
    page: 1,
    limit: 2
  };
  categoryModel.paginate({}, paginateOptions).then(function(categoryData) {
    res.render('categories', { 
      title: 'Category List', 
      categories: categoryData, 
      current: categoryData.pagingCounter,
      pages: categoryData.totalPages
    });
  });
});

router.get('/:page', chkLogin, function(req, res, next) {
  var paginateOptions = {
    page: req.params.page,
    limit:    2
  };
  categoryModel.paginate({}, paginateOptions).then(function(categoryData) {
    res.render('categories', { 
      title: 'Category List', 
      categories: categoryData, 
      current: categoryData.page,
      pages: categoryData.totalPages
    });
  });
});

router.get('/delete/:id', chkLogin, function(req, res, next) {
  let catID = req.params.id;  
  catDel = categoryModel.findByIdAndDelete(catID);
  catDel.exec(function(err){
    if(err) throw err;
    res.redirect('/categories');
  });
});

module.exports = router;
