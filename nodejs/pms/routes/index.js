var express = require('express');
const bodyParser = require('body-parser');
const { check, validationResult } = require('express-validator');
const { matchedData, sanitizeBody } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
var chkLogin = require('../modules/chk-login');
var userModel = require('../modules/user');
var router = express.Router();

// create application/json parser
//const jsonParser = bodyParser.json(); 
// create application/x-www-form-urlencoded parser
//const urlencodedParser = bodyParser.urlencoded({ extended: false });

const user = userModel.find({});

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

/* GET Login page. */
router.get('/', function(req, res, next) {
  let logintoken = localStorage.getItem('userToken');
  if(logintoken){
    res.redirect('dashboard');
  }else{
    res.render('index', { title: 'PMS', errs: '', frmVal: '' });
  }  
});

router.post('/', [
  check('unm', 'required').isLength({min: 1})
  .custom(value => {
    return userModel.findOne({name: value}).then(user => {
      if (!user) {
        return Promise.reject('User not register');
      }
    })
  }),
  check('upw', 'required').isLength({min: 1})
  .custom((value, { req }) => {
    return userModel.findOne({name: req.body.unm }).then(user => {
      if (!bcrypt.compareSync(value, user.password)) {        
        return Promise.reject('invalid password');
      }
    })
  })
], function(req, res, next) {
  const errors = validationResult(req);
  var userFrm = matchedData (req);
  if(!errors.isEmpty()){
    res.render('index', { 
        title: 'Login',
        errs: errors.mapped(),
        frmVal: userFrm,
    });
  }else{
    let userLogin = userModel.findOne({name: req.body.unm});
    userLogin.exec((err, userData) => {
      if(err) throw err;
      var token = jwt.sign({ userID: userData._id }, 'loginToken');
      localStorage.setItem('userToken', token);
      res.redirect('/dashboard');
    });    
  }  
});

/* GET Dashboard page. */
router.get('/dashboard', chkLogin, function(req, res, next) {
  userCount = userModel.count();
  userCount.exec((err, getUserCount) => {  
    res.render('dashboard', { title: 'PMS Dashboard', userCount: getUserCount });

  });
});

/* GET Logout page. */
router.get('/logout', function(req, res, next) {
  localStorage.removeItem('userToken');
  res.redirect('/');
});

/* GET Signup page. */
router.get('/signup', function(req, res, next) {
  res.render('signup', { title: 'Register', errs: '', frmVal: '', sux: '' });
});

router.post('/signup', [
  check('unm', 'required').isLength({min: 1})
  .custom(value => {
    return userModel.findOne({name: value}).then(user => {
      if (user) {
        return Promise.reject('User name already exist.');
      }
    });
  }),
  check('ueml', 'must be email').isEmail()
  .custom(value => {
    return userModel.findOne({email: value}).then(eml => {
      if (eml) {
        return Promise.reject('email already exist.');
      }
    });
  }),
  check('upw').isLength({ min: 3 }).withMessage('must be at least 3 chars long')
  .custom((value, { req }) => {
    if (value !== req.body.ucpw) {
      throw new Error('Password don\'t match');
    }else{
      return value;
    }
  })
], function(req, res, next) {
  const errors = validationResult(req);
  var userFrm = matchedData (req);
  if(!errors.isEmpty()){
    res.render('signup', { 
        title: 'Register',
        errs: errors.mapped(),
        frmVal: userFrm,
    });
  }else{
    addUser = new userModel ({
      name: req.body.unm,
      email: req.body.ueml,
      password: bcrypt.hashSync(req.body.upw, 10), 
    });
    addUser.save(function(err, reply){
      if(err) throw err;
      // res.redirect('/signup');
      res.render('signup', { title: 'Register', errs: '', frmVal: '', sux: 'Register Successfully.' });
    });
  }  
});

module.exports = router;