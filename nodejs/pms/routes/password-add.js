var express = require('express');
const bodyParser = require('body-parser');
const { check, validationResult } = require('express-validator');
const { matchedData, sanitizeBody } = require('express-validator');
// const bcrypt = require('bcryptjs');
// const jwt = require('jsonwebtoken');
var chkLogin = require('../modules/chk-login');
var categoryModel = require('../modules/category');
var passwordModel = require('../modules/password');
var router = express.Router();

const categories = categoryModel.find({});
const passwords = passwordModel.find({});

// if (typeof localStorage === "undefined" || localStorage === null) {
//   var LocalStorage = require('node-localstorage').LocalStorage;
//   localStorage = new LocalStorage('./scratch');
// }

/* GET Category-add page. */
router.get('/', chkLogin, function(req, res, next) {
  categories.exec((err, categoriesData) => {
    res.render('password-add', { title: 'Add Password', categories: categoriesData, errs: '', frmVal: '', sux: '' });
  });  
});

router.post('/', chkLogin, [
  check('prjnm', 'required').isLength({min: 1}),
  check('pwdtl', 'required').isLength({min: 1})
], function(req, res, next) {
  const errors = validationResult(req);
  var catFrm = matchedData (req);
  if(!errors.isEmpty()){
    categories.exec((err, categoriesData) => {
      res.render('password-add', { 
        title: 'Add password',
        categories: categoriesData,
        errs: errors.mapped(),
        frmVal: catFrm,
        sux: ''
      });
    });
    
  }else{
    let pwdID = req.body.id;
    if(pwdID){
      let updateRecord = passwordModel.findOneAndUpdate({ _id: pwdID }, {category_id: req.body.ctid, project: req.body.prjnm, details: req.body.pwdtl});
      updateRecord.exec((err, reply) => {
        if(err) throw err;
        res.redirect('/passwords');
      });
    }else{
      addPassword = new passwordModel ({
        category_id: req.body.ctid,
        project: req.body.prjnm,
        details: req.body.pwdtl
      });
      addPassword.save(function(err, reply){
        if(err) throw err;
        categories.exec((err, categoriesData) => {
          res.render('password-add', { 
            title: 'Add password',
            categories: categoriesData,
            errs: '',
            frmVal: '',
            sux: 'Added Successfully'
          });
        });
      });
    }
  }  
});

router.get('/edit/:id', chkLogin, function(req, res, next) {
  let pwdID = req.params.id;
  let pwdRecord = passwordModel.findOne({ _id: pwdID });  
  pwdRecord.exec(function(err, record){
    if(err) throw err;
    let pwdVals = {id: record._id, ctid: record.category, prjnm: record.project , pwdtl: record.details};
    categories.exec((err, categoriesData) => {
      res.render('password-add', { title: 'Edit Password', categories: categoriesData, errs: '', frmVal: pwdVals, sux: '' });
    });    
  });  
});

module.exports = router;