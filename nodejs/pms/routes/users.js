var express = require('express');
var userModel = require('../modules/user');
var chkLogin = require('../modules/chk-login');
var router = express.Router();

var user = userModel.find({});

/* GET users listing. */
router.get('/', chkLogin, function(req, res, next) {
  user.exec(function(err, userData){
    if(err) throw err;
    res.render('users', { title: 'Users', users: userData });
  });
});

module.exports = router;
