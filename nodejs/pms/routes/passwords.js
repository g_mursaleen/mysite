var express = require('express');
const bodyParser = require('body-parser');
// const { check, validationResult } = require('express-validator');
// const { matchedData, sanitizeBody } = require('express-validator');
// const bcrypt = require('bcryptjs');
// const jwt = require('jsonwebtoken');
var chkLogin = require('../modules/chk-login');
var passwordModel = require('../modules/password');
var router = express.Router();

const passwords = passwordModel.find({});

// if (typeof localStorage === "undefined" || localStorage === null) {
//   var LocalStorage = require('node-localstorage').LocalStorage;
//   localStorage = new LocalStorage('./scratch');
// }

/* GET Passwords page. */
router.get('/', chkLogin, function(req, res, next) {
  var perPage = 3;
  var page = req.params.page || 1;

  let getPasswords = passwordModel.aggregate([
    {
      $lookup:
        {
          from: 'categories',
          localField: 'category_id',
          foreignField: '_id',
          as: 'category_detail'
        }
    },
    {$unwind: '$category_detail'}
  ]).skip((perPage * page) - perPage).limit(perPage);

  getPasswords.exec(function(err, passwordsData){
    if(err) throw err;
    passwordModel.countDocuments({}).exec((err, count) => {
      res.render('passwords', { 
        title: 'Passwords List', 
        passwords: passwordsData, 
        current: page,
        pages: Math.ceil(count / perPage)
      });
    });    
  });
});

router.get('/:page', chkLogin, function(req, res, next) {
  var perPage = 3;
  var page = req.params.page || 1;

  let getPasswords = passwordModel.aggregate([
    {
      $lookup:
        {
          from: 'categories',
          localField: 'category_id',
          foreignField: '_id',
          as: 'category_detail'
        }
    },
    {$unwind: '$category_detail'}
  ]).skip((perPage * page) - perPage).limit(perPage);

  getPasswords.exec(function(err, passwordsData){
    if(err) throw err;
    passwordModel.countDocuments({}).exec((err, count) => {
      res.render('passwords', { 
        title: 'Passwords List', 
        passwords: passwordsData, 
        current: page,
        pages: Math.ceil(count / perPage)
      });
    });    
  });
});
 
router.get('/delete/:id', chkLogin, function(req, res, next) {
  let pwdID = req.params.id;  
  pwdDel = passwordModel.findByIdAndDelete(pwdID);
  pwdDel.exec(function(err){
    if(err) throw err;
    res.redirect('/passwords');
  });
});

module.exports = router;
