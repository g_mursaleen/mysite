var express = require('express');
const bodyParser = require('body-parser');
const { check, validationResult } = require('express-validator');
const { matchedData, sanitizeBody } = require('express-validator');
// const bcrypt = require('bcryptjs');
// const jwt = require('jsonwebtoken');
var chkLogin = require('../modules/chk-login');
var categoryModel = require('../modules/category');
var router = express.Router();

const categories = categoryModel.find({});

// if (typeof localStorage === "undefined" || localStorage === null) {
//   var LocalStorage = require('node-localstorage').LocalStorage;
//   localStorage = new LocalStorage('./scratch');
// }

/* GET Category-add page. */
router.get('/', chkLogin, function(req, res, next) {
  res.render('category-add', { title: 'Add Category', errs: '', frmVal: '', sux: '' });
});

router.post('/', chkLogin, [
  check('ctnm', 'required').isLength({min: 1})
  .custom(value => {
    return categoryModel.findOne({name: value}).then(category => {
      if (category) {
        return Promise.reject('Already exist.');
      }
    });
  })
], function(req, res, next) {
  const errors = validationResult(req);
  var catFrm = matchedData (req);
  if(!errors.isEmpty()){
    res.render('category-add', { 
      title: 'Add Category',
      errs: errors.mapped(),
      frmVal: catFrm,
      sux: ''
    });
  }else{
    let catID = req.body.ctid;
    if(catID){
      let updateRecord = categoryModel.findOneAndUpdate({ _id: catID }, {name: req.body.ctnm});
      updateRecord.exec((err, reply) => {
        if(err) throw err;
        res.redirect('/categories');
      });
    }else{
      addCategory = new categoryModel ({
        name: req.body.ctnm, 
      });
      addCategory.save(function(err, reply){
        if(err) throw err;
        res.render('category-add', { title: 'Add Category', errs: '', frmVal: '', sux: 'Register Successfully.' });
      });
    }
  }  
});

router.get('/edit/:id', chkLogin, function(req, res, next) {
  let catID = req.params.id;
  let catRecord = categoryModel.findOne({ _id: catID });  
  catRecord.exec(function(err, record){
    if(err) throw err;
    let catVals = {id: record._id, ctnm: record.name};
    res.render('category-add', { title: 'Edit Category', errs: '', frmVal: catVals, sux: '' });
  });  
});

module.exports = router;