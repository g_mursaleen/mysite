const jwt = require('jsonwebtoken');

/* Login with localstorage and json web token. */
function chkLogin (req, res, next) {
  let logintoken = localStorage.getItem('userToken');
  try {
    jwt.verify(logintoken, 'loginToken');
  } catch(err) {
    res.redirect('/');
  }
  next();
}

module.exports = chkLogin;