var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
mongoose.connect('mongodb://localhost:27017/pms', {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});
var conn = mongoose.connection;

var categorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: {
      unique: true
    }
  },
  created_at: {
    type: Date,
    default: Date.now
  },
});

categorySchema.plugin(mongoosePaginate);
var categoryModel = mongoose.model('categories', categorySchema);

module.exports = categoryModel;