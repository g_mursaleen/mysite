var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/pms', {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});
var conn = mongoose.connection;

var passwordSchema = new mongoose.Schema({
  category_id: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  project: {
    type: String,
    required: true
  },
  details: {
    type: String,
    required: true
  },
  created_at: {
    type: Date,
    default: Date.now
  },
});

var passwordModel = mongoose.model('passwords', passwordSchema);

module.exports = passwordModel;