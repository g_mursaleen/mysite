<?php include('include/header.php'); ?>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
      <h1 class="fw400 text-center text-uppercase">Portfolio</h1>
      <hr style="border-color:#666;">
      <p class="lead text-center">Please Follow below link for more detail</p>
      
      <?php
	    /*$cxm_qty = 29;
		$cxm_ii = 1; 
	    for($cxm_i = 30; $cxm_i < 100; $cxm_i += 30){
			//echo $cxm_i.'<hr>';
			
			if( $cxm_qty <= $cxm_i ){		
				$new_cost = 18.85;
				echo $cxm_i .' | '. $new_cost * $cxm_ii.'<hr>';
				$cxm_ii++;
			}
			
		}*/
	  ?>
      
      <div class="row">
        <div class="col-sm-6">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/web-bw.png">
            <div class="anchor"><a href="projects.php">Online Projects</a></div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/web-bw.png">
            <div class="anchor"><a href="web-app.php">Web Application</a></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/mobile-bw.png">
            <div class="anchor"><a href="mob-app.php">Mobile Application</a></div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/email-bw.png">
            <div class="anchor"><a href="email.php">HTML email</a></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/logo-bw.png">
            <div class="anchor"><a href="logos.php">Logos</a></div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/card-bw.png">
            <div class="anchor"><a href="business-card.php">Business Card</a></div>
          </div>
        </div>
      </div>      
    </div>
  </div>
</div>
<?php include('include/footer.php'); ?>
</body>
</html>
