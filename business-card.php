<?php include('include/header.php'); ?>
<body>
<div class="container">
  <h1 class="fw400 text-center text-uppercase">Business Card</h1>
  <hr style="border-color:#666;">
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/1-card.png" target="_blank"><img alt="Business Card" src="media/business-card/1-card.png"></a> </div>
        </div>
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/2-card.png" target="_blank"><img alt="Business Card" src="media/business-card/2-card.png"></a> </div>
        </div>
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/3-card.png" target="_blank"><img alt="Business Card" src="media/business-card/3-card.png"></a> </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/4-card.png" target="_blank"><img alt="Business Card" src="media/business-card/4-card.png"></a> </div>
        </div>
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/5-card.png" target="_blank"><img alt="Business Card" src="media/business-card/5-card.png"></a> </div>
        </div>
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/6-card.png" target="_blank"><img alt="Business Card" src="media/business-card/6-card.png"></a> </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/7-card.png" target="_blank"><img alt="Business Card" src="media/business-card/7-card.png"></a> </div>
        </div>
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/8-card.png" target="_blank"><img alt="Business Card" src="media/business-card/8-card.png"></a> </div>
        </div>
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/9-card.png" target="_blank"><img alt="Business Card" src="media/business-card/9-card.png"></a> </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/10-card.png" target="_blank"><img alt="Business Card" src="media/business-card/10-card.png"></a> </div>
        </div>
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/11-card.png" target="_blank"><img alt="Business Card" src="media/business-card/11-card.png"></a> </div>
        </div>
        <div class="col-sm-4">
          <div class="well well-sm well-gm text-center"> <a href="media/business-card/12-card.png" target="_blank"><img alt="Business Card" src="media/business-card/12-card.png"></a> </div>
        </div>
      </div>
    </div>
  </div>
  <?php include('include/categories.php'); ?>
</div>
<?php include('include/footer.php'); ?>
</body>
</html>
