<div class="cxm-cats">
  <div class="row">
    <div class="col-sm-12">
      
      <div class="row">
        <div class="col-sm-2">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/web-bw.png">
            <div class="anchor"><a href="projects.php">Online Projects</a></div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/web-bw.png">
            <div class="anchor"><a href="web-app.php">Web Application</a></div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/mobile-bw.png">
            <div class="anchor"><a href="mob-app.php">Mobile Application</a></div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/email-bw.png">
            <div class="anchor"><a href="email.php">HTML email</a></div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/logo-bw.png">
            <div class="anchor"><a href="logos.php">Logos</a></div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="well well-sm well-gm text-center"> <img class="img-responsive center-block" src="media/card-bw.png">
            <div class="anchor"><a href="business-card.php">Business Card</a></div>
          </div>
        </div>
      </div>
      
      <div class="well well-sm well-gm anchor text-center"><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></div>
    </div>
  </div>
</div>
